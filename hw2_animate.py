#!/usr/bin/env python
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import scipy.misc as misc
from glob import glob

IMAGE_FILES = []

def GetFiles(prefix="triangle"):
	global IMAGE_FILES
	fn = '%s*.png' % (prefix)
	IMAGE_FILES = glob(fn)
	IMAGE_FILES.sort()

def UpdateFig(ii):
	im = plt.imshow(misc.imread(IMAGE_FILES[ii]))
	return im,

GetFiles(prefix="test")
fig = plt.figure()
im = plt.imshow(misc.imread(IMAGE_FILES[0]))
ani = animation.FuncAnimation(fig,UpdateFig,frames=range(len(IMAGE_FILES)),interval=10,blit=True)

plt.show()
print IMAGE_FILES

