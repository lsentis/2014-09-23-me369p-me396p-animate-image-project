import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import scipy.ndimage as ndimage
import scipy.misc as misc
from glob import glob

def GenerateRandomTriangle(sigma=0):
	vertices = GenerateVertices()
	triangle_image = np.zeros((256,256))
	for ii in range(256):
		for jj in range(256):
			point = np.array(GetXYIndex([ii,jj],256))
			if IsInside(point, vertices):
				triangle_image[ii,jj] = 1
	triangle_image = ndimage.gaussian_filter(triangle_image, sigma)
	return triangle_image

def GenerateVertices():
	vertices = (64*np.random.random((3,2))).astype(np.int)
	vertices[0,:] = -vertices[0,:]
	vertices[1,0] = 0
	vertices[2,1] = -vertices[2,1]
	return vertices

def IsInside(point,vertices):
	inside = True
	for ii in range(3):
		v0tov1 = vertices[ii,:]-vertices[ii-1,:]
		v0top0 = point - vertices[ii-1,:]
		cp = np.cross(v0top0,v0tov1)
		if cp < 0:
			inside = False
			return inside
	return inside

def IsClosest(point, vertices):
	pl = np.outer(np.ones(len(vertices[:,0])),point) 
	dx = pl[:,0] - vertices[:,0]
	dy = pl[:,1] - vertices[:,1]
	return np.argmin(np.multiply(dx,dx) + np.multiply(dy,dy))

def GetXYIndex(pixel,size):
	return [pixel[0] - size/2, size/2 - pixel[1]]

def SpinAndSave(image,rate,time,nimages=10,prefix="triangle"):
	total_rotation = rate * time * 360 / np.pi
	for ii in range(0,nimages):
		angle = total_rotation * (ii / float(nimages))
		im = ndimage.rotate(image,angle,mode='constant', reshape=False)
		fn = '%s_%03d.png' % (prefix,ii)
		misc.imsave(fn,im)

def GetFileList(prefix="triangle"):
	fn = '%s*.png' % (prefix)
	filelist = glob(fn)
	filelist.sort()
	return 	filelist

def GetFileListAsArray(prefix="triangle"):
	filelist = GetFileList(prefix)
	im0 = misc.imread(filelist[0]).astype(np.float) #Should this be float?
	imsize = im0.shape
	print im0.shape[0]
	stacked_images = np.zeros((len(filelist),imsize[0],imsize[1]))
	print stacked_images.shape
	stacked_images[0,:,:] = im0
	for ii in range(1,len(filelist)):
		im = misc.imread(filelist[ii]).astype(np.float)
		stacked_images[ii,:,:] = im

	return stacked_images

def GetSimpleVertexImage(image):
	opened_image = ndimage.binary_opening(image)
	return image - opened_image

def GetWindowIndices(centerPixel, windowSize, xmin=0, xmax=255, ymin=0, ymax=255):
	iix

def GetHarrisVertexImage(image, kappa=0.05, windowSize=9, windowSpace=2, sigma=2):
	Ix = ndimage.sobel(image, axis=0, mode='constant')
	Iy = ndimage.sobel(image, axis=1, mode='constant')
	
	#For easier bookeeping, make the window size odd
	if windowSize%2 == 0:
		windowSize = windowSize+1
	if windowSpace > windowSize/2.:
		windowSpace = (windowSize-1)/2
	
	gaussian_weight = np.zeros((windowSize,windowSize))
	center = (windowSize+1)/2-1
	gaussian_weight[center,center] = 1
	gaussian_weight = ndimage.gaussian_filter(gaussian_weight,sigma)

	harrisIm = np.zeros((256,256))
	iix = center
	iiy = center
	width = (windowSize-1)/2
	
	for iix in range(center,len(image[1,:])-center,windowSpace):
		for iiy in range(center,len(image[:,1])-center,windowSpace):
			xinds = np.arange(iix-width,iix-width+windowSize)
			yinds = np.arange(iiy-width,iiy-width+windowSize)
			print 'center: (%i,%i), width: %i, windowSize: %i'%(iix,iiy,width,windowSize)
			print 'xinds: ' 
			print xinds
			print 'yinds: '
			print yinds
			#TODO: CLEAN UP INDEXING
			Ix_window = Ix[iix-width:iix+width+1,iiy-width:iiy+width+1]
			Iy_window = Iy[iix-width:iix+width+1,iiy-width:iiy+width+1]
			Ix2_window = np.multiply(Ix_window,Ix_window)
			Iy2_window = np.multiply(Iy_window,Iy_window)
			IxIy_window = np.multiply(Ix_window,Iy_window)
			M = np.zeros((2,2))
			M[0,0] = np.sum(np.multiply(Ix2_window,gaussian_weight))
			M[1,1] = np.sum(np.multiply(Iy2_window,gaussian_weight))
			M[0,1] = np.sum(np.multiply(IxIy_window,gaussian_weight))
			M[1,0] = M[0,1]
			#TODO: CLEAN UP THIS INDEXING
			harrisIm[iix-windowSpace:iix+windowSpace,
					 iiy-windowSpace:iiy+windowSpace] = np.linalg.det(M) - kappa * np.trace(M)**2

	# plt.imshow(harrisIm)
	# plt.show(block=True)
	return harrisIm

def GetLucasKanadFlowFields(image1, image2, dt, windowSize=7, windowSpace=2, sigma=2):
	Ix = ndimage.sobel(image1, axis=0, mode='constant')
	Iy = ndimage.sobel(image1, axis=1, mode='constant')
	It = (image2 - image1) / dt
	
	if windowSize%2 == 0:
		windowSize = windowSize+1
	if windowSpace > windowSize/2.:
		windowSpace = (windowSize-1)/2
	
	gaussian_weight = np.zeros((windowSize,windowSize))
	center = (windowSize+1)/2-1
	gaussian_weight[center,center] = 1
	gaussian_weight = ndimage.gaussian_filter(gaussian_weight,sigma)

	Vx = np.zeros((256,256))
	Vy = np.zeros((256,256))
	A = np.zeros((2,2))
	b = np.zeros(2)
	iix = center
	iiy = center
	width = (windowSize-1)/2

	for iix in range(center,len(image1[1,:])-center,windowSpace):
		for iiy in range(center,len(image1[:,1])-center,windowSpace):
			Ix_window = Ix[iix-width:iix+width+1,iiy-width:iiy+width+1]
			Iy_window = Iy[iix-width:iix+width+1,iiy-width:iiy+width+1]
			It_window = It[iix-width:iix+width+1,iiy-width:iiy+width+1]
			Ix2_window = np.multiply(Ix_window,Ix_window)
			Iy2_window = np.multiply(Iy_window,Iy_window)
			IxIy_window = np.multiply(Ix_window,Iy_window)
			IxIt_window = np.multiply(It_window,Ix_window)
			IyIt_window = np.multiply(It_window,Iy_window)
			
			A[0,0] = np.sum(np.multiply(Ix2_window,gaussian_weight))
			A[1,1] = np.sum(np.multiply(Iy2_window,gaussian_weight))
			A[0,1] = np.sum(np.multiply(IxIy_window,gaussian_weight))
			A[1,0] = A[0,1]
			b[0] = -np.sum(np.multiply(IxIt_window,gaussian_weight))
			b[1] = -np.sum(np.multiply(IyIt_window,gaussian_weight))
			if(np.linalg.det(A)>1e-5):
				v = np.linalg.solve(A,b)
				Vx[iix-windowSpace:iix+windowSpace, iiy-windowSpace:iiy+windowSpace] = v[0]
				Vy[iix-windowSpace:iix+windowSpace, iiy-windowSpace:iiy+windowSpace] = v[1]
	
	return [Vx, Vy]

def GetVertexMask(image, threshold=0.1, dilation_struct=np.ones((4,4)), label_struct=np.ones((3,3))):
	norm_image = image/np.max(image)
	mask_image = norm_image>threshold
	mask_image = ndimage.binary_dilation(mask_image, structure = dilation_struct)
	label_image, n_labels = ndimage.label(mask_image, structure = label_struct)

	#Well crud...what if this doesn't work?
	vertices = np.zeros((n_labels,2))
	for ii in range(1,n_labels+1):
		vinds = np.argwhere(label_image==ii)
		vertices[ii-1,:] = [int(np.mean(vinds[:,0])), int(np.mean(vinds[:,1]))]

	return [vertices, mask_image, label_image]

def GetMeanValue(image, centerPixel, windowSize = 5):

	if windowSize%2 == 0:
		windowSize = windowSize+1
	if windowSpace > windowSize/2.:
		windowSpace = (windowSize-1)/2

	width = (windowSize-1)/2
	start_x = max(centerPixel[0]-width,0)
	stop_x = min(centerPixel[0]+width+1,len(image[0,:])-1)
	start_y = max(centerPixel[1]-width,0)
	stop_y = min(centerPixel[1]+width+1,len(image[:,0])-1)
	print 'center: %i, start: %i, stop: %i' % (centerPixel[0], start_x, stop_x)	
	return np.mean(image[start_x:stop_x, start_y:stop_y])


def main():

	triangle_image = GenerateRandomTriangle(sigma=0)
	SpinAndSave(triangle_image, 4, 2, nimages = 100, prefix="test2")
	images = GetFileListAsArray("test2")

	flist = GetFileList(prefix="test2")
	im1 = misc.imread(flist[0]).astype(np.float)
	im2 = misc.imread(flist[1]).astype(np.float)

	im1 = im1/np.max(im1)
	im2 = im2/np.max(im2)

	simple_vertex_image = GetSimpleVertexImage(im1)
	[simple_v_inds, simple_mask, simple_label_image] = GetVertexMask(simple_vertex_image)
	plt.figure(figsize=(12,5))
	plt.subplot(121)
	plt.imshow(simple_vertex_image, cmap=plt.cm.gray)
	plt.title('Simple Vertex Image')
	plt.subplot(122)
	plt.imshow(simple_label_image)
	plt.title('Labeled')
	plt.show(block=True)

	harris_vertex_image = GetHarrisVertexImage(im1)
	[v0, mask, label_image] = GetVertexMask(harris_vertex_image)
	plt.figure(figsize=(12,5))
	plt.subplot(121)
	plt.imshow(harris_vertex_image, cmap=plt.cm.gray)
	plt.title('Harris Vertex Image')
	plt.subplot(122)
	plt.imshow(label_image)
	plt.title('Labeled')
	plt.show(block=True)	
	
	dt = 2/100.
	[Vx, Vy] = GetLucasKanadFlowFields(im1, im2, dt)
	[Vxm, Vym] = GetLucasKanadFlowFields(np.multiply(mask,im1), np.multiply(mask,im2), dt)

	plt.figure(figsize=(10, 10))
	plt.subplot(221)
	plt.imshow(np.multiply(mask,Vx))
	plt.title('Vx', fontsize=20)
	plt.subplot(222)
	plt.imshow(np.multiply(mask,Vy))
	plt.title('Vy', fontsize=20)
	plt.subplot(223)
	plt.imshow(np.multiply(mask,Vxm))
	plt.title('Vxm', fontsize=20)
	plt.subplot(224)
	plt.imshow(np.multiply(mask,Vym))
	plt.title('Vym', fontsize=20)
	plt.show()

	nimages = images.shape[0]
	time = dt*np.array(range(0,nimages-1))
	v_lables = np.array(range(0,3))
	p_traj = np.zeros((nimages-1,3,2))
	v_traj = np.zeros((nimages-1,3,2))
	
	v_last = v0
	
	for ii in range(0,nimages-1):
		print ii
		im1 = images[ii,:,:].astype(np.float)
		im2 = images[ii+1,:,:].astype(np.float)
		
		im1_hv = GetHarrisVertexImage(im1) 
		[vertices, mask, labels] = GetVertexMask(im1_hv)

		v_lables_temp = v_lables
		for jj in range(0,len(v_lables_temp)):
			cind = IsClosest(v_last[v_lables_temp[jj],:],vertices)
			if not cind == v_lables_temp[jj]:
				v_lables[jj] = cind

		p_traj[ii,:,:] = vertices[v_lables,:]
		v_last = vertices

		#TODO: Better tracking using velocities
		#tolernace for how much a point can move?
		#[Vx, Vy] = GetLucasKanadFlowFields(im1, im2, dt)
		# for vii in range(0,len(vinds[:,0])):
		# meanvelx = GetMeanValue(Vx,vinds[0,:].astype(np.int))
		# meanvely = GetMeanValue(Vy,vinds[0,:].astype(np.int))
		# p0_traj[ii,0] = meanvelx
		# p0_traj[ii,1] = meanvely
	
	p_traj.tofile('p_traj2.out')	

	p_traj = np.fromfile('p_traj2.out').reshape(nimages-1,3,2)
	print p_traj.shape

	plt.figure(figsize=(5, 5))
	plt.plot(time[range(0,len(p_traj[:,0,0]))],p_traj[:,0,0],'r',
			 time[range(0,len(p_traj[:,0,0]))],p_traj[:,1,0],'b',
			 time[range(0,len(p_traj[:,0,0]))],p_traj[:,2,0],'g')
	plt.show()

	
	print 'done!'

if __name__ == '__main__':
	main()

